(ns example6
  (:require [dfast.io :refer [load-loans load-recovery-rates
                              load-transition-matrix report]]
            [dfast.loans :refer :all :as loan]
            [dfast.simulation :refer [local-rand path-chunks]])
  (:import  [cern.jet.random.tdouble Normal]))

;; TODO: currently has some "8 ratings" assumptions built in

(def ll (->> (load-loans "ll.csv")
             (map assoc-loan-ead)))
(def rr (load-recovery-rates    "resources/RecoveryRate.csv"))
(def tm (load-transition-matrix "resources/Transition_matrix.csv"))
;; (set! *warn-on-reflection* true) ;; important for ensuring  aget speed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data for expected Loss (EL)

(defn make-el-func [tm rr]
  (let [rtg->pd (make-rtg->pd tm)     ;; Probability of Default (PD) for each rating
        sec->lgd (make-sec->lgd rr)]  ;; Loss Given Default (LGD) means per collateral type
    (fn el [l]
      (* (:bal l)
         (rtg->pd (:rating l))
         (sec->lgd (:security l)))))) 

(defn make-lossfunc
  "make a loss function from a recovery rate matrix"
  [rr]
  (let [slmap (into {} (map (fn [[k i]]
                              (let [rr-row (nth (:rows rr) i)
                                    mean (- 1 (:Mean rr-row))
                                    std-dev   (:StdDev rr-row)]
                                [k #(Normal/staticNextDouble mean std-dev)]))
                            (:index rr)))]
    (fn lossfunc [rtg] ((slmap rtg)))))

;; expected loss on portfolio
(println (format "Expected loss direct calc: %,.0f\n" (transduce (map (make-el-func tm rr)) + ll)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now for monte-carlo simulation of losses

(defn tm->i8p8
  [tm]
  (->> (for [row-map (:rows tm)
             :let [ ;;sorting index sorted descening of vector
                   [sortidx sorted] (->> (:column-names tm)
                                         (map-indexed (fn [i x] [i (row-map x)]))
                                         (sort-by (comp - second))
                                         (apply map vector))]]
         [sortidx (-> (reductions + sorted) ;;cum sum
                      (butlast)             ;;but make sure it ends with 1.0
                      (concat [1.0]))])
       ;;transpose to gather indices (aka rankings) and pct's together
       (apply map vector)
       ;;turn 2x8x8 vec into 2x64 vec
       (map (partial apply concat)) ;; or (map flatten) for brevity
       ((juxt (comp byte-array first)
               (comp double-array second)))))


;; may change but current rand it not thread local (contention issues)

(defn new-rtg
  [old-rtg ^doubles p8 ^bytes i8]
  (let [x (local-rand)]
    (loop [i (* 8 (int old-rtg))]
      (if (<= x (aget p8 i))
        (aget i8 i)
        (recur (inc i))))))

(defn losses
  "take portfolio recovery matrix transition matrix and number-of-paths
  perform monte-carlo and return sample losses"
  [ll rr tm  num-paths]
  (let [lll (map #(update % :rating (:index tm)) ll)
        lgdfunc (make-lossfunc rr)
        [i8 p8] (tm->i8p8 tm)]
    (repeatedly num-paths
                #(transduce (comp (filter (fn [l] (= 7 (new-rtg (:rating l) p8 i8))))
                                  (map (fn [l] (* (first (:ead l))
                                                  (lgdfunc (:security l))))))
                            +
                            lll))))

(time (report (losses ll rr tm (* 100 1000))))


(time (->> (path-chunks 100000 (.availableProcessors (Runtime/getRuntime)))
           (pmap #(doall (losses ll rr tm %)))
           (apply concat)
           report))
