(ns example2
  (:require [dfast.loans :refer :all :as loan]
            [dfast.cc-config :as cc]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :refer [xml-> xml1-> attr]])
  (:import [java.io File FileInputStream]
           [java.util.zip GZIPInputStream])) 


(def t (-> "resources/ccruncher/test100.xml"
                            FileInputStream.
                            xml/parse
                            zip/xml-zip))

(def subdir "resources/ccruncher")
(def z (-> (File. subdir "test100.xml")
           FileInputStream.
           xml/parse
           zip/xml-zip))

;; navigate to a node edit it and show root result
(def p (-> (xml1-> z
                   :portfolio)
           (zip/edit cc/include subdir)
           (zip/root)
           ;; That gets the right root this is just for display capture
           ((fn [r] (with-out-str (xml/emit r))))))


;; TODO: move to tests...can they be individual tests (i.e. fixtures)
(doall
 (for [f (.listFiles (File. "resources" "ccruncher"))
       :when (re-matches #"test.*\.xml" (.getName f))]
   (do
     (print "\nloading file: " (.getName f))
     (print (:trans (cc/load-cfg (.getPath f)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; experiment with pre-defined randoms for testing and dev
(let [a (volatile! [0.1 0.25 0.75 0.333 0.6667 0.999]) i (volatile! -1)]
  (defn not-so-random-rand
    ([]
     (@a (vswap! i #(mod (inc %) (count @a)))))
    ([new-a]
     (vreset! i -1)
     (vreset! a new-a))))

(comment
;this doesn't work but should with clojure.data.xml rather than clojure.xml
(-> (str "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
         "<foo><bar><baz>The baz value</baz></bar></foo>")
    (java.io.StringReader.)
    (xml/parse))
;this do work
(-> (.getBytes (str "<?xml version='1.0' encoding='UTF-8'?>"
                    "<foo bar='34'>"
                    "<baz qux='quizzle'/>"
                    "</foo>"))
    io/input-stream
    xml/parse)
;; show report for a ccruncer generates losses set
(-> (io/read-dataset "../../runs/ccruncher/output/portfolio.csv"
                     :header true
                     :comment-char "#")
    (i/sel :cols :portfolio)
    (report :show-histogram true))



)
