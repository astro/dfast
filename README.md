# dfast

A Clojure library for Monte-Carlo simulation based calculation for Loan portfolio losses.

  - EL Expected Loss
  - VaR Value at Risk 
  - ES Expected Shortfall 

## License

Copyright © 2016 Openvest Research
