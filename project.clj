(defproject dfast "0.1.0-SNAPSHOT"
  :description "CCAR and Dodd Frank Stress testing"
  :url "https://bitbucket.org/astro/dfast"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/data.zip "0.1.2"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/tools.cli "0.4.1"]
                 [org.clojure/core.async  "0.4.490"]
                 [incanter "1.5.7"]
                 [progrock "0.1.2"]]
  :main dfast.core
  :profiles {:dev {:dependencies [[org.clojure/test.check "0.9.0"]
                                  [criterium "0.4.4"]
                                  ;[ring/ring-spec "0.0.4"]
                                  ]
                   :plugins [;[org.clojars.benfb/gorilla-repl "0.5.3"]
                             [lein-gorilla "0.4.0"]
                             ]}}
  ;; -server not needed for windows64  not sure about others 
  :jvm-opts ^:replace ["-server"]  ;; why does this appear slower  should inline more???
)
