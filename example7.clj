(ns example7
  (:require [dfast.io :refer [load-loans load-recovery-rates
                              load-transition-matrix report]]
            [dfast.loans :refer [make-rtg->pd make-sec->lgd 
                                 set-amortization-frequency]]
            [dfast.simulation :refer [local-rand path-chunks]])
  (:import  [cern.jet.random.tdouble Normal]))


(set! *warn-on-reflection* true) ;; important for ensuring  aget speed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data for expected Loss (EL)

(defn make-el-func [tm rr]
  (let [rtg->pd (make-rtg->pd tm)     ;; Probability of Default (PD) for each rating
        sec->lgd (make-sec->lgd rr)]  ;; Loss Given Default (LGD) means per collateral type
    (fn el [l]
      (* (:bal l)
         (rtg->pd (:rating l))
         (sec->lgd (:security l)))))) 

(defn make-lossfunc
  "make a loss function from a recovery rate matrix"
  [rr]
  (let [slmap (into {} (map (fn [[k i]]
                              (let [rr-row (nth (:rows rr) i)
                                    mean (- 1 (:Mean rr-row))
                                    std-dev   (:StdDev rr-row)]
                                [k #(Normal/staticNextDouble mean std-dev)]))
                            (:index rr)))]
    (fn lossfunc [rtg] ((slmap rtg)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now for monte-carlo simulation of losses

(defn tm-row->fn
  "takes: the map rating->idx (e.g. {:aaa 0 :aa 1 ...})
          and 1 row from tm
   returns: a function with noargs
            returns: the next rating"
  [rtg->idx row]
  (let [[rtgs pcts] (->> (sort-by (comp - second) row)  ;; sort
                         (apply map vector))            ;; and separate
        rtgs  (map rtg->idx rtgs)  ;; use numbers not keywords for rtg
        pcts  (-> (reductions + (butlast pcts))  ;; cum pcts
                  (concat [1.0]))]               ;; force cum pct to 1 (rounding)
    (eval `(fn [] (condp >= (local-rand) ~@(interleave pcts rtgs))))))


(defn mk-rtg-xform
  "takes: a transition matrix (tm) 
   returns: a function that
            takes: initial rating index (e.g. 1 for aa)
                   number of periods to iterate over 
            returns: next rating index (e.g. 7 for default)"
  [tm]
  (let [default-idx (dec (count (:rows tm)))
        fn-array (mapv (partial tm-row->fn (:index tm)) (:rows tm))]
    (fn [rtg-idx times]
      (loop [n 0
             rtg rtg-idx]
        (cond (= default-idx rtg) (- n)
              (>= n times) rtg
              :else (recur (inc n) ((fn-array rtg))))))))

(defn losses
  "take portfolio recovery matrix transition matrix and number-of-paths
  perform monte-carlo and return sample losses"
  [lll rrr tm  num-paths num-periods]
  (let [lgdfunc (make-lossfunc rrr)
        xform (mk-rtg-xform tm)]
    (repeatedly num-paths         ;; "only" 50% slower if we pass r rather than update l  
                                  ;; 2x slower with multiperiod
                #(transduce (comp (map    (fn [l] [(xform (get l :rating) num-periods) l]))
                                  (filter (fn [[r l]] (neg? r)))
                                  (map    (fn [[r l]] (* (nth (:ead l) (dec (- r)))
                                                         (lgdfunc (:security l))))))
                            +
                            lll))))

(defn base-losses
  "take portfolio recovery matrix transition matrix and number-of-paths
  perform monte-carlo and return sample losses"
  [{:keys [loans transitions recovery num-paths num-periods frequency cores]
    :as options}]
  (let [rrr (load-recovery-rates recovery)
        tmm (load-transition-matrix transitions)
        lll (->> (load-loans loans)
                 (map #(set-amortization-frequency % frequency)) ;;e.g. monthly or quartly
                 (map #(update % :rating (:index tmm))))] ;;make rating numeric
    (->> (path-chunks num-paths cores)
         (pmap #(doall (losses lll rrr tmm % num-periods)))
         (apply concat))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; not just try out a few runs
(def run1 {:loans "ll.csv"
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/Transition_matrix.csv"
           :frequency 1
           :num-periods 1
           :num-paths 100000
           :cores 3})

(def run2 {:loans "ll.csv",
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/tpm/A6.csv"
           :frequency 1
           :num-periods 1
           :num-paths 100000
           :cores 3})

(def run3 {:loans "ll.csv"
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/tpm/A3.csv"
           :frequency 12
           :num-periods 12
           :num-paths 100000
           :cores 3})

(time (report (base-losses run1)))
(time (report (base-losses run2)))
(time (report (base-losses run3)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment
  ;; above returns a function like:
  (fn []
    (let [x (local-rand)]
      (condp >= x
        0.9065 :dfast.loans/aa
        0.9844 :dfast.loans/a
        0.9914 :dfast.loans/aaa
        0.9978 :dfast.loans/bbb
        0.9992 :dfast.loans/b
        0.9998 :dfast.loans/bb
        0.9999 :dfast.loans/ccc
        1.0 :dfast.loans/default)))
;; condp is a macro so it expands to nested if statements

  )
