(ns example3
  (:require [dfast.io :refer [load-loans load-recovery-rates
                              load-transition-matrix report]]
            [dfast.loans :refer :all :as loan]))

;; a more streamilned version of example1
;; (set! *warn-on-reflection* true)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; read in some data from ifiles
(def ll (load-loans             "ll.csv"))
(def rr (load-recovery-rates    "resources/RecoveryRate.csv"))
(def tm (load-transition-matrix "resources/Transition_matrix.csv"))

;; (def ll (map assoc-loan-ead ll))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data for expected Loss (EL)

;; Probability of Default (PD) for each rating
(def rtg->pd (make-rtg->pd tm))

;; Loss Given Default (LGD) means used for Expected Loss (EL)
(def sec->lgd (make-sec->lgd rr))

;; expected loss on a loan l rr rtg->pd
(defn el [l]
  (* (:bal l)
     (rtg->pd (:rating l))
     (sec->lgd (:security l)))) 

(defn sl [l]
  (let [rr-row (nth (:rows rr) ((:security l) (:index rr)))]
   (* (:bal l)
      (- 1 (sample-recovery rr-row)))))

;; expected loss on portfolio
(println (format "Expected loss direct calc: %,.0f\n" (transduce (map el) + ll)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now for monte-carlo simulation of losses

;; sample loss should converge to el for large num-path
(def losses
  (let [num-path 10000
        xform-rtg (transform-rating3 tm)]
    (repeatedly num-path
                #(transduce (comp (map xform-rtg)
                                  (filter (fn [l] (= :dfast.loans/default (:rating l))))
                                  (map sl))
                            + ll))))
;; this is to force eval and get a sense of timing (~100K per second initially)
;; now ~300k per second after moving from transform-matrix to transform-matrix2
(time (report losses))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; there is quite a bit here but this version does parallelize
(time (report
            (let [num-path 100000
                  cores (.availableProcessors (Runtime/getRuntime))
                  xform-rtg (transform-rating3 tm)
                  losses-fn (fn [] (repeatedly (/ num-path cores)
                                               #(transduce (comp (map xform-rtg)
                                                                 (filter (fn [l] (= :dfast.loans/default (:rating l))))
                                                                 (map sl))
                                                           + ll)))]
              (->> losses-fn
                   (comp doall)
                   (repeat cores)
                   (apply pcalls)
                   (apply concat)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

