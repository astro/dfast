(ns dfast.cc-config-test
  (:require [clojure.test :refer :all]
            [dfast.cc-config :refer :all]))

;; this could use serious TLC but it's a start.
(deftest load-configs
  (doseq [fname (->> (file-seq (java.io.File. "resources/ccruncher"))
                     (filter #(re-matches #"test.*\.xml" (.getName %))))]
    (let [config (load-cfg (.getPath fname))]
      (is (contains? config :portfolio) "hey where's the portfolio")
      (is (contains? config :trans)))))
