(ns dfast.loans-test
  (:require [clojure.test :refer :all]
            [dfast.loans :refer :all :as l]))

(deftest pmt-m
  (testing "test payment calc"
    (is (->> (pmt 150000 0.04 15 12)
             (format "%.2f")
             (= "1109.53" )))))

(deftest sort-rnks
  (testing "sort rankings"
    (is (= [::l/aaa ::l/aa+ ::l/aa ::l/aa-
            ::l/a ::l/b ::l/c ::l/default]
           (sort-by full-rnk->int [::l/c ::l/aaa ::l/a ::l/aa+ ::l/aa ::l/default ::l/aa- ::l/b])))))
