(ns example6
  (:require [dfast.io :refer [load-loans load-recovery-rates
                              load-transition-matrix report]]
            [dfast.loans :refer [make-rtg->pd make-sec->lgd 
                                 set-amortization-frequency]]
            [dfast.simulation :refer [local-rand path-chunks]])
  (:import  [cern.jet.random.tdouble Normal]))


;; TODO: currently has some "8 ratings" assumptions built in

;; (set! *warn-on-reflection* true) ;; important for ensuring  aget speed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data for expected Loss (EL)

(defn make-el-func [tm rr]
  (let [rtg->pd (make-rtg->pd tm)     ;; Probability of Default (PD) for each rating
        sec->lgd (make-sec->lgd rr)]  ;; Loss Given Default (LGD) means per collateral type
    (fn el [l]
      (* (:bal l)
         (rtg->pd (:rating l))
         (sec->lgd (:security l)))))) 

(defn make-lossfunc
  "make a loss function from a recovery rate matrix"
  [rr]
  (let [slmap (into {} (map (fn [[k i]]
                              (let [rr-row (nth (:rows rr) i)
                                    mean (- 1 (:Mean rr-row))
                                    std-dev   (:StdDev rr-row)]
                                [k #(Normal/staticNextDouble mean std-dev)]))
                            (:index rr)))]
    (fn lossfunc [rtg] ((slmap rtg)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now for monte-carlo simulation of losses

(defn tm->i8p8
  "input: transition matrix
  returns: two arrays
              - cumulative pct sorted with highest first (e.g. 98 99 99.5 1)
              - rating corresponding to the percentages  (e.g. :a :b :c :default)"
  [tm]
  (->> (for [row-map (:rows tm)
             :let [ ;;sorting index sorted descening of vector
                   [sortidx sorted] (->> (:column-names tm)
                                         (map-indexed (fn [i x] [i (row-map x)]))
                                         (sort-by (comp - second))
                                         (apply map vector))]]
         [sortidx (-> (reductions + sorted) ;;cum sum
                      (butlast)             ;;but make sure it ends with 1.0
                      (concat [1.0]))])
       ;;transpose to gather indices (aka rankings) and pct's together
       (apply map vector)
       ;;turn 2x8x8 vec into 2x64 vec
       (map (partial apply concat)) ;; or (map flatten) for brevity
       ((juxt (comp byte-array first)
              (comp double-array second)))))



(defn new-rtg
  "bond is not currently in default"
  ([^doubles p8 ^bytes i8 old-rtg max-periods]
   (new-rtg p8 i8 old-rtg (int max-periods) (int 0)))
  ([^doubles p8 ^bytes i8 old-rtg max-periods num-periods]
   (cond (= 7 old-rtg) (- num-periods)
         (>= num-periods max-periods)  old-rtg
         :default (let [x (local-rand)]
                    (loop [i (* 8 (int old-rtg))]
                      (if (<= x (aget p8 i))
                        (new-rtg p8 i8 (aget i8 i) max-periods (inc num-periods))
                        (recur (inc i))))))))

(defn losses
  "take portfolio recovery matrix transition matrix and number-of-paths
  perform monte-carlo and return sample losses"
  [lll rrr tm  num-paths num-periods]
  (let [lgdfunc (make-lossfunc rrr)
        [i8 p8] (tm->i8p8 tm)]
    (repeatedly num-paths         ;; "only" 50% slower if we pass r rather than update l  
                                  ;; 2x slower with multiperiod
                #(transduce (comp (map    (fn [l] [(new-rtg p8 i8 (get l :rating) num-periods) l]))
                                  (filter (fn [[r l]] (neg? r)))
                                  (map    (fn [[r l]] (* (nth (:ead l) (dec (- r)))
                                                         (lgdfunc (:security l))))))
                            +
                            lll))))

(defn base-losses
  "take portfolio recovery matrix transition matrix and number-of-paths
  perform monte-carlo and return sample losses"
  [{:keys [loans transitions recovery num-paths num-periods frequency cores]
    :as options}]
  (let [rrr (load-recovery-rates recovery)
        tmm (load-transition-matrix transitions)
        lll (->> (load-loans loans)
                 (map #(set-amortization-frequency % frequency)) ;;e.g. monthly or quartly
                 (map #(update % :rating (:index tmm))))] ;;make rating numeric
    (->> (path-chunks num-paths cores)
         (pmap #(doall (losses lll rrr tmm % num-periods)))
         (apply concat))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; not just try out a few runs
(def run1 {:loans "ll.csv",
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/Transition_matrix.csv",
           :frequency 1,
           :num-periods 1,
           :num-paths 100000,
           :cores 3})

(def run2 {:loans "ll.csv",
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/tpm/A6.csv",
           :frequency 1,
           :num-periods 1,
           :num-paths 100000,
           :cores 3})

(def run3 {:loans "ll.csv",
           :recovery "resources/RecoveryRate.csv"
           :transitions "resources/tpm/A3.csv",
           :frequency 12,
           :num-periods 12,
           :num-paths 100000,
           :cores 3})

(time (report (base-losses run1)))
(time (report (base-losses run2)))
(time (report (base-losses run3)))
