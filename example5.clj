(ns example5
  (:require [dfast.io :refer [load-loans load-recovery-rates
                              load-transition-matrix report]]
            [dfast.loans :refer :all :as loan]
            [incanter.core :as i])
  (:import (java.util.concurrent ThreadLocalRandom)))


(def ll (->> (load-loans "ll.csv")
             (map assoc-loan-ead)))
(def rr (load-recovery-rates    "resources/RecoveryRate.csv"))
(def tm (load-transition-matrix "resources/Transition_matrix.csv"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data for expected Loss (EL)

;; Probability of Default (PD) for each rating
(def rtg->pd (make-rtg->pd tm))

;; Loss Given Default (LGD) means used for Expected Loss (EL)
(def sec->lgd (make-sec->lgd rr))


;; expected loss on a loan l rr rtg->pd
(defn el [l]
  (* (:bal l)
     (rtg->pd (:rating l))
     (sec->lgd (:security l)))) 

(defn sl [l]
  (let [rr-row (nth (:rows rr) ((:security l) (:index rr)))]
    (* (first (:ead l))
      (- 1 (sample-recovery rr-row)))))

;; expected loss on portfolio
(println (format "Expected loss direct calc: %,.0f\n" (transduce (map el) + ll)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now for monte-carlo simulation of losses
 
;; sample loss should converge to el for large num-path
;; currently 5x slower than transform3 but this does abunch of assoc 
;; not done there (e.g. maturity and ead)
(defn transform-rating5 [tm]
  "From a transition matrix:
      - creates a map of rtg->function to calc new rating (with simulation)
      - gets rating from loan, finds func, calls func assoc new rating back in"
  (let [rtg->xf (into {}
                      (for [[rtg i] (:index tm)]
                        (let [[idx prob] (->> (i/sel tm  :rows i)
                                              (map-indexed vector)
                                              (sort-by (comp - second))
                                              (apply (partial map vector)))
                              prob (vec (reductions + prob))
                              rtgs (mapv (:column-names tm) idx)]
                          [rtg  (fn [r]
                                  (loop [i 0]
                                    (if (>= (prob i) r)
                                      (rtgs i)
                                      (recur (inc i)))))])))]
    (fn [l r] 
      ;reducing function taking a loan and a random number
      ;  i.e. reduce over a series of random nums
      ;  stop when maturity = 0 or loan defaults
      (let [new-rtg ((rtg->xf (:rating l)) r)]
        ;;(println "from " (:rating l) " to " new-rtg) ;; for dev
        (if (= new-rtg :dfast.loans/default)
          (-> l
              (assoc :rating :dfast.loans/default) ;; calc ead?
              (reduced))
          (-> l
              (assoc :rating new-rtg)
              ;(update :ead rest) ;; only at default?
              (update :maturity dec)
              ((fn [l] (if (zero? (:maturity l))
                         (reduced l)
                         l)))))))))

;; may change but current rand is not thread local (contention issues)
(def local-rand
  (let [r (ThreadLocalRandom/current)]
    #(.nextDouble r)))


(defn losses
  "for loans ll and transition matrix tm, 
   calculates num-paths losses transforming each loan num-periods of times
   TODO: does not match example3 i.e. (losses ll tm 10000 1)"
  [ll tm num-paths num-periods] ;; pass in sl also?
  (let [tm5 (transform-rating5 tm)]
    ;; returns num-paths vectors of loans reduced num-period times
    (->> ll
         (map (fn [l]
                (repeatedly num-paths
                            (fn [] ;; use reduction function tm5 to reduce loan over num-periods 
                              (reduce tm5 l
                                      (repeatedly num-periods
                                                  local-rand)))))
              )
         (apply map vector)
         (map  #(transduce (comp (filter (fn [l] (= :dfast.loans/default (:rating l))))
                                 (map sl))
                           + %)))))

(time (report (losses ll tm 10000 1)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; there is quite a bit here but this version does parallelize
(time (report
            (let [num-path 100000
                  num-periods 1
                  cores (.availableProcessors (Runtime/getRuntime))
                  losses-fn (fn [] (losses ll tm (/ num-path cores) num-periods))]
              (->> losses-fn
                   (comp doall)
                   (repeat cores)
                   (apply pcalls)
                   (apply concat)))))

(comment
  (loop [per (int (min (:maturity l) num-periods))
         rtg (:rating l)]
            (if (zero? per) ;; (or (zero? per) (= 7 rtg))
                (if (= rtg 7)
                    (- num-periods per)
                    rtg)
                (recur (dec per) (new-rtg l))))
)
