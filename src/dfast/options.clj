(ns dfast.options)
(require '[incanter.stats :as stats])
(import java.util.Random)
#_(set! *warn-on-reflection* true)

;; numbers adapted to match the example from the "Python for Finance"
;; Yves Hipisch python version
;; https://books.google.com/books?id=7tzSBQAAQBAJ&pg=PT870&lpg=PT870&dq=option+monte+carlo+105+sigma+S0&source=bl&ots=wXDo0dnRAe&sig=2fWd19Ny1qtcBc5H9nWX5Z3S3jE&hl=en&sa=X&ved=0ahUKEwiYj-vd8sHRAhUD7iYKHfyIAXQQ6AEIIzAC#v=onepage&q=option%20monte%20carlo%20105%20sigma%20S0&f=false
(let [S0 100.0
      K 105.0
      T 1.0
      r 0.05
      sigma  0.2
      I 100000
      sqrtT (* sigma (Math/sqrt T))
      ss (* T (- r (* 0.5 (Math/pow sigma 2))))]
    (->> (stats/sample-normal I)
         (map (fn [z] (* S0 (Math/exp (+ ss (* z sqrtT))))))
         (map (fn [ST] (max (- ST K) 0)))
         (reduce +)
         (* (Math/exp (* T (- 0 r))) (/ 1 I))))

(let [S0 100.0
      K 105.0
      T 1.0
      r 0.05
      sigma  0.2
      I 100000
      sqrtT (* sigma (Math/sqrt T))
      ss (* T (- r (* 0.5 (Math/pow sigma 2))))]
    (->> (stats/sample-normal I)
         (map (fn [z] (* S0 (Math/exp (+ ss (* z sqrtT))))))
         (map (fn [ST] (max (- ST K) 0)))
         (reduce +)
         (* (Math/exp (* T (- 0 r))) (/ 1 I))))


(time (let [S0 100.0
            K 105.0
            T 1.0
            r 0.05
            sigma  0.2
            I 100000
            sqrtT (* sigma (Math/sqrt T))
            ss (* T (- r (* 0.5 (Math/pow sigma 2))))]
        (->> (stats/sample-normal I)
             (map (fn [z] (-> (* z sqrtT)
                              (+ ss)
                              (Math/exp)
                              (* S0)
                              (- K)
                              (max 0))))
             (reduce +)
             (* (Math/exp (* T (- 0 r))) (/ 1 I)))))

(time (let [S0 100.0
                    K 105.0
                    T 1.0
                    r 0.05
                    sigma  0.2
                    I 100000
                    sqrtT (* sigma (Math/sqrt T))
                    ss (* T (- r (* 0.5 (Math/pow sigma 2))))]
        (-> (transduce 
             (map (fn [z] (-> (* z sqrtT)
                              (+ ss)
                              (Math/exp)
                              (* S0)
                              (- K)
                              (max 0))))
             +
             (stats/sample-normal I)
             #_(let [r (Random.)] (for [_ (range I)] (.nextGaussian r))))
            (* (Math/exp (* T (- 0 r))))
            (/ I))))
