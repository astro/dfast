(ns dfast.io
  (:require [incanter.core :as i]
            [incanter.io :as io]
            [incanter.stats :as stats]
            [incanter.charts :refer [histogram]]
            [dfast.loans :refer [map->amortized-loan adjust-tm ccc-c]]))

;; util
(defn index-dataset
  "add :index to dataset based on the first column"
  [dataset]
  (conj (i/$ [:not 0] dataset)
        [:index
         (into {} (map-indexed
                   ;; this takes the ns from where it's being run, not here.
                   ;; not sure what practice is best.
                   ;; #(vector (keyword (str *ns*)  ; make namespaced keywords with - for " "
                   #(vector (keyword (str "dfast.loans")  ; make namespaced keywords with - for " "
                                     (.toLowerCase (.replace %2 " " "-")))
                            %1)
                   (i/$ 0 dataset)))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; loading 

(defn load-loans
  "load loan portfolio from dsv"
  [fname]
  (->> (io/read-dataset fname :header true)
       :rows
       ;;(take 5)
       ;; make ratings and securitization namespaced keywords
       (map #(update % :rating (partial keyword "dfast.loans")))
       (map #(update % :security (partial keyword "dfast.loans")))
       (map map->amortized-loan)))

(defn load-recovery-rates
  [fname]
  (-> fname
      (io/read-dataset :header true :comment-char "#")
      (index-dataset)))

(defn load-transition-matrix
  [fname]
  (let [tm       (io/read-dataset fname :header true :comment-char "#")
        AAA->aaa (into {} (map #(vector % (keyword "dfast.loans"
                                                   (.toLowerCase (name %))))
                               (:column-names tm)))]
    (-> (i/rename-cols AAA->aaa tm)
        (index-dataset)
        (ccc-c)
        (adjust-tm))))

;; reporting
(defn report
  [losses & {:keys [show-histogram] :as params} ]
  (let [cutoff (stats/quantile losses :probs 0.99)]
    (print
     (format (str "Expected Loss %,.0f\n"
                  "Value at Risk: %,.0f\n"
                  "Expected Shortfall: %,.0f\n"
                  "Simulations: %,d\n")
             (stats/mean losses) ;; should we use this sample or use the calculated number from above?
             cutoff
             (stats/mean (filter #(> % cutoff) losses ))
             (count losses)))
    (if show-histogram
      (i/view (histogram losses :nbins 100)))))

(defn save-loans
  [loans fname]
  (->> loans
       (map #(into {} %))              ;; turn records into maps
       (map #(dissoc % :cash-flows))   ;; remove cash flows
       (map #(dissoc % :ead))          ;; remove ead series
       (map #(update % :rating name))  ;; change namespaced keywords to strings
       (map #(update % :security name))
       i/to-dataset
       (#(i/save % fname))))

