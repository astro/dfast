(ns dfast.core
  (:require [clojure.string :as string]
            [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(def cli-options
  [["-n" "--num-paths " "Number of paths"
    :default 10000 
    :parse-fn #(Integer/parseInt %)]
   ["-c" "--cores " "Number of cores to use"
    :default (.availableProcessors (Runtime/getRuntime))
    :parse-fn #(Integer/parseInt %)]
   ["-f" "--frequency " "int payments per year (12 => monthly)"
    :default 1
    :parse-fn #(Integer/parseInt %)]
   ["-p" "--num-periods " "number of periods forward"
    :default 1
    :parse-fn #(Integer/parseInt %)]
   ["-r" "--recovery " "Recovery rates (Mean & StdDev) for lgd"
    :default "resources/RecoveryRate.csv"]
   ["-t" "--transitions " "Transition percents Matrix"
    :default "resources/Transition_matrix.csv"]
   ["-h" "--help"]])

(defn usage [options-summary]
  (string/join \newline
               ["Usage: program-name [options] portfolio.csv"
                ""
                "Options:"
                options-summary]))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]}
        (parse-opts args cli-options)]
    ;; Handle help and error conditions
    (cond
      (:help options)            (exit 0 (usage summary))
      (not= (count arguments) 1) (exit 1 (usage summary))
      errors                     (exit 1 (error-msg errors)))
    ;; Execute program with options
    (println options arguments)))
