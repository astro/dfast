(ns dfast.simulation)

;; may change but current rand is not thread local (contention issues)
(def local-rand
  (let [r (java.util.concurrent.ThreadLocalRandom/current)]
    #(.nextDouble r)))

(defn path-chunks
   "return a sequence of (/ paths cores) with rounding correction
   e.g. (path-chunks 100 3) => (34 33 33)"
  [paths cores]
  (if (pos? cores)
    (let [chunk (int (Math/ceil (/ paths cores)))]
      (cons chunk
            (lazy-seq (path-chunks (- paths chunk) (dec cores)))))))
