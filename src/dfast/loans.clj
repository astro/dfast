(ns dfast.loans
  (:require [clojure.spec.alpha :as s]
            [incanter.core :as i]))

(defn pmt
  "Calculate loan payment
  (pmt 150000 0.04 15 12) -> 1109.53
  150K loan 4% 15 years monthly payment => 1109.53 payment"

  ([{:keys [amt rate maturity freq]}]  ;; called with a loan
   (pmt amt rate maturity 1))

  ([prinicpal rate term freq]  ;; called with params
   (let [rtpp (/ rate freq)]  ;; rate per period
     (* prinicpal (/ rtpp (- 1 (Math/pow (+ 1 rtpp) (* -1 term freq))))))))

(defprotocol ILoan
;  (calc-loss [this severity]) 
  (make-pmt
    [this]
    ;[this pmt]
    )
  (do-default [loan]))

(defrecord BulletLoan [bal rate maturity rating security]
  ILoan
  ;; (calc-loss [loan severity] (* pd lgd amt))
  (make-pmt
      [loan] 
    (if-let [p (first (:cash-flows loan))]
      (-> loan
          (update :collected + p)
          (update :cash-flows rest)
          (update :maturity dec)
          (update :bal -  (- p (* rate bal))))
      (dissoc loan :cash-flows)))
  (do-default [loan]
    (assoc loan :rating ::default
                :maturity 0
                :cash-flows (list bal)
                :status :default)))

(defn ->bullet-loan
  "constructor for BulletLoan that sets the orig balance"
  [bal rate maturity rating security]
  (-> (->BulletLoan bal rate maturity rating security)
      (assoc :collected 0
             :orig-bal bal
             :cash-flows (concat (repeat (dec maturity) (* rate bal))
                                 [(* bal (+ 1 rate))])))) 

(defrecord AmortizedLoan [bal rate maturity rating security]
  ILoan
  ;; (calc-loss [loan severity] (* pd lgd ead))
  ;; (make-pmt [loan] (make-pmt loan (:pmt loan)))
  (make-pmt
      [loan] 
    (if-let [p (first (:cash-flows loan))]
      (-> loan
          (update :collected + p)
          (update :cash-flows rest)
          (update :maturity dec)
          (update :bal -  (- p (* rate bal))))
      (dissoc loan :cash-flows)))
  (do-default
      [loan]
    (assoc loan :rating ::default
                :maturity 0
                :cash-flows (list bal)
                :status :default)))


(defn ->amortized-loan
  "constructor for AmortizedLoan that sets the orig balance"
  [bal rate maturity rating security]
  (-> (->AmortizedLoan bal rate maturity rating security)
      (assoc :orig-bal bal
             :collected 0
             :cash-flows (repeat maturity
                                 (pmt bal rate maturity 1)))))

(defn map->amortized-loan
  "constructor for AmortizedLoan that sets the orig balance"
  [{:keys [bal rate maturity rating] :as loan-map}]
  (-> (map->AmortizedLoan loan-map)
      (assoc :orig-bal bal
             :collected 0
             :cash-flows (repeat maturity
                                 (pmt bal rate maturity 1)))))

#_(-> {:amt 100, :rating :aaa, :pd 0.1, :ead 50.0, :lgd 0.2}
    (map->Loan)
    (calc-loss :SEV))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some maps to translate one rating format to another

(def rnk->int (into {} (map vector  [::aaa ::aa ::a 
                                     ::bbb ::bb ::b 
                                     ::ccc ::cc ::c 
                                     ::default] (range))))

(def full-rnk->int (into {} (map vector [::aaa        ::aa+ ::aa ::aa- ::a+ ::a ::a- 
                                  ::bbb+ ::bbb ::bbb- ::bb+ ::bb ::bb- ::b+ ::b ::b- 
                                  ::ccc+ ::ccc ::ccc- ::cc+ ::cc ::cc- ::c+ ::c ::c- 
                                  ::default] (range))))
(def full-RNK->int (into {} (map vector [::AAA        ::AA+ ::AA ::AA- ::A+ ::A ::A-
                                  ::BBB+ ::BBB ::BBB- ::BB+ ::BB ::BB- ::B+ ::B ::B- 
                                  ::CCC+ ::CCC ::CCC- ::CC+ ::CC ::CC- ::C+ ::C ::C- 
                                  ::DEFAULT] (range))))
(def full-Rnk->int (into {} (map vector [::Aaa         ::Aa1 ::Aa2 ::Aa3 ::A1 ::A2 ::A3
                                  ::Baa1 ::Baa2 ::Baa3 ::Ba1 ::Ba2 ::Ba3 ::B1 ::B2 ::B3 
                                  ::Caa1 ::Caa2 ::Caa3 ::Ca1 ::Ca2 ::Ca3 ::C1 ::C2 ::C3 
                                  ::Default] (range))))

(defn ccc-c
  "some datasets only go thru ccc.
  This adds cc and c with ccc values to index."
  [ds]
  (if-let [ccc (get-in ds [:index ::ccc])]
    (-> ds
        (assoc-in [:index ::cc] ccc)
        (assoc-in [:index ::c] ccc))
    ds))

(defn make-rtg->pd 
  "extract map of rating to  probabilty of default from transition dataset"
  [tm]
  (into {}
        (map (fn [[k v]] [k (i/$ v ::default tm)]))
        (:index tm)))

(defn make-sec->lgd 
  "extract map of rating to Mean loss given default (LGD) percentage
  from recovery rates"
  [rr]
  (into {}
        (map (fn [[r i]] [r (- 1 (i/$ i :Mean rr))]))
        (:index rr)))

(defn assoc-loan-ead
  "use make-pmt to iterate through time and
   calculate vector of ead (spot balances)
   assoc back into loan as :ead"
  [l]
  (loop [bal (:bal l) ;; TODO: maybe loop on (dec maturity)?
         ball [bal]
         l1 l]
    (if (< bal 1) ;; rounding
      (assoc l :ead (butlast ball))
      (let [l1 (make-pmt l1)
            bal1 (:bal l1)]
        (recur bal1 (conj  ball bal1) l1)))))

(defn set-amortization-frequency
  "translate an amortized loan to a frequency of:
    - annual (1)
    - quarterly (4)
    - or monthly (12)" 
  [l frequency]
  (-> l
      (update :maturity * frequency)
      (update :rate / frequency)
      (assoc  :frequency frequency)
      map->amortized-loan
      assoc-loan-ead))

(defn transform-ratings [tmx l]
  (->> (i/mmult (:rtg-mx l) tmx)
       (assoc l :rtg-mx)))

(defn adjust-tm
  "adjust transition matrix last column so that values sum to l
   otherwise sampling can throw errors"  ;; TODO: maybe set last col to 1?
  [tm]
  (let [adj-col (last (:column-names tm))]
    (assoc tm :rows
           (map #(update % adj-col + (reduce - 1 (vals %)))
                (:rows tm)))))
(defn adjust-tm1
  "adjust transition matrix last column so that values sum to l
   otherwise sampling can throw errors"
  [tm]
  (let [adj-col (last (:column-names tm))]
    (assoc tm :rows
           (map #(update % adj-col inc) (:rows tm)))))


(defn init-cashflows [{:keys [amt rate] :as l}]
  (-> (repeat (dec (:maturity l)) (* amt rate))
      (vec)
      (conj (* amt (+ 1 rate)))
      (i/matrix)
      (i/trans)))

(defn assoc-init-cashflows [l]
  (assoc l :cash-flows (init-cashflows l)))

(defn index-dataset
  "add :index to dataset based on the first column"
  [dataset]
  (conj (i/$ [:not 0] dataset)
        [:index
         (into {} (map-indexed
                   ;; this takes the ns from where it's being run, not here.
                   ;; not sure what practice is best.
                   ;; #(vector (keyword (str *ns*)  ; make namespaced keywords with - for " "
                   #(vector (keyword (str "dfast.loans")  ; make namespaced keywords with - for " "
                                     (.toLowerCase (.replace %2 " " "-")))
                            %1)
                   (i/$ 0 dataset)))]))

(defn pv-loan [rtg->df loan]
  (let [discount-factors ((:rating loan) rtg->df) ]
    (reduce + (:collected loan 0)
              (map * (:cash-flows loan)
                     discount-factors))))

;; Discount factors from forward curves
;;;;;;;;;;;;; Infinite series of discount factors ;;;;;;;;;;;;;;;;;;;;;;
;; (for [r (div 1 (plus 1 (to-matrix c)))]                            ;;
;;   (take 10 (concat (map pow (butlast r) (iterate inc 1))           ;;
;;                    (map (partial pow (last r)) (iterate inc 4))))) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn curves->df 
  "conversion that takes: dataset of ratings by spot rates
                 returns: map of ratings to discount factors "
  [curves]
  (let [df (i/div 1 (i/plus 1 (i/to-matrix curves)))]
    (into {}
          (for [[rtg ix] (:index curves)]
            (let [r (i/$ ix :all df)]
              [rtg (concat (map i/pow (butlast r) (iterate inc 1))
                           ;; take 12 here is to just keep repl from hanging
                           (take 12 (map (partial i/pow (last r))
                                         (iterate inc 4))))])))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  These functions are "non-pure" returning sampeling
(require '[incanter.stats :as stats])

(defn sample-recovery
  [{:keys [Mean StdDev]}]
  (-> (stats/sample-normal 1 :mean Mean :sd StdDev)
      (min 1)
      (max 0)))

(defn transform-rating [tm l]
  (let [ratings (:column-names tm)
        probs (i/sel tm :rows ((:index tm) (:rating l)))]
    (assoc l :rating (first (stats/sample-multinomial 1 :categories ratings :probs probs)))))

(defn transform-rating2 [tm]
  (let [ratings (:column-names tm)
        rtg->probs (->> (:index tm)
                        (map (fn [[k v]] [k (i/sel tm :rows v)]))
                        (into {}))]
    (fn xform-rtg [l]
      (->> (:rating l)
           (rtg->probs)
           (stats/sample-multinomial 1 :categories ratings :probs)
           (first)
           (assoc l :rating)))))

(defn transform-rating3 [tm]
  "From a transition matrix:
      - creates a map of rtg->function to calc new rating (with simulation)
      - gets rating from loan, finds func, calls func assoc new rating back in"
  (let [;; map of rating to function that get a rand and lookup new rating
        rtg->xf (into {}
                      (for [[rtg i] (:index tm)]
                        (let [[idx prob] (->> (i/sel tm  :rows i)
                                              (map-indexed vector)
                                              (sort-by (comp - second))
                                              (apply (partial map vector)))
                              prob (vec (reductions + prob))
                              rtgs (mapv (:column-names tm) idx)]
                          [rtg  (fn []
                                  (let [x (rand)]
                                    (loop [i 0]
                                      (if (>= (prob i) x)
                                        (rtgs i)
                                        (recur (inc i))))))])))]
    (fn  [l]
      (->> (:rating l)
           rtg->xf
           (#(%))
           (assoc l :rating)))))
;maybe migrate to another random number generator.
;this might give us the ability to seed and also, as per java docs:
;When applicable, use of ThreadLocalRandom rather than shared Random
; objects in concurrent programs will typically encounter much less overhead
; and contention.  TEST first.  might already be used????

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; finally some clojure spec stuff
;; (s/def ::amt (s/and number? #(>= % 0)))
;; (s/def ::amt (s/and number? (complement neg?) #(< % 1e50)))
;; (s/def ::amt (s/and number? pos?))
(s/def ::bal (s/and number? #(<= 0 % 1e9)))
(s/def ::big-bal (s/and number? #(< 1000 % 1e30) ))
(s/def ::rate (set (range 0.04 0.06 0.005)))
(s/def ::maturity #{2 3 4 5})
(s/def ::rating #{::aaa ::aa ::a 
                  ::bbb ::bb ::b 
                  ::ccc ::cc ::c 
                  ::default})  ;; nillable?
(s/def ::security #{::senior-secured ::senior-unsecured ::senior-subordinated
                    ::subordinated ::junior-subordinated})
(s/def ::pd (s/and float? #(<= 0.0 % 1.0)))
(s/def ::ead (s/and number? #(<= 0 % 1e30)))
(s/def ::lgd (s/and float? #(<= 0.0 % 1.0)))

(s/def ::lob #{::CCard ::IB ::BB ::CB ::Mtg ::Other})
(s/def ::geo #{::NAMER ::EUR ::PAC ::OTHER})

;; returns a loan as a map
;; (s/def ::loan (s/keys :req-un [::bal ::rate ::rating ::maturity ::security]))
;; returns a loans as a list
(s/def ::loan (s/and (s/cat :bal (s/and ::bal #(> % 500))
                            :rate ::rate  ;; interest, 0.06 => 6%
                            :maturity ::maturity  
                            :rating ::rating
                            :security ::security)
                     #_(fn [{:keys [amt ead]}]
                           (> amt ead))))

  ;; (s/explain ::loan {::amt 30 ::rating ::aaa ::pd 0.01 ::ead 10 ::lgd 0.5})
  ;; (calc-loss (->Loan 100 :aaa 0.1 50.0 0.2) :SEV)

(comment
  (require '[clojure.spec.gen.alpha :as gen])
  (gen/generate (s/gen ::loan))
  (apply ->AmortizedLoan (gen/generate (s/gen ::loan)))
  (def ll (->> (s/exercise ::loan 5)
               (map (comp map->amortized-loan second))
               (filter #(not= ::default (:rating %)))))

  (defn expand 
    "use map m to multiply elements of map l
     like: select columns then matrix multiply (for 1 row/record)"
    [m l]
    (reduce (fn [tot [k v]] (+ tot (* v (k l)))) 0 m))

  (let [m {:amt 10 :pd 20 :ead 1.5 :lgd 3}]
    (map (partial expand m) (take 2 ll)))

  (let [m {:amt 10 :pd 20 :ead 1.5 :lgd 3}]
    (map (fn [l] (assoc l :loss (expand m l))) (take 2 ll)))

  (let [m {:amt 10 :pd 20 :ead 1.5 :lgd 3}]
    (map (fn [l] (assoc l :loss (calc-loss l m))) (take 2 ll)))
)

