(ns dfast.cc-config
  (:require [incanter.core :as i]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :refer [xml->]]
            [clojure.xml :as xml]
            [clojure.string :as s])
  (:import [java.io File FileInputStream]
           [java.util.zip GZIPInputStream] ))


(defn parse-pct
  "parseFloat but will handle \"3.4%\""
  [s]
  (if-let [n (second (re-matches #"([+-]?[0-9.]+)%" s))]
    (/ (Double/parseDouble n) 100.0)
    (Double/parseDouble s)))

(defn rename-key
  "rename a key e.g. :id to :otherid as id is reused at multiple levels"
  [old new m]
  (if-let [v (get m old)]
    (-> m (dissoc old) (assoc new v))
    m))

(defn onode->alist 
  "given an obligor node retrieve all assets and give them each
   the obligors attributes"
  [onode]
  (let [o (->> onode
               zip/node
               xml/attrs
               (rename-key :id :oid))
        p+o (fn [p] (merge o p))
        params (fn [node] (->> node
                               zip/node
                               xml/attrs
                               (rename-key :id :aid)))]
    (map p+o (xml-> onode :asset params))))

(defn snode->slist
  [snode]
  (let [s (->> snode zip/node xml/attrs)
        slist (apply hash-map
                     (xml-> snode
                            :segment
                            (comp (juxt (comp keyword :name)
                                        identity)
                                  xml/attrs
                                  zip/node)))]
    (assoc s :segments slist)))


(defn include
  [node subdir]
  (if-let [fname (get-in node [:attrs :include])]
    (-> (File. subdir fname)
        FileInputStream.
        (#(if (s/ends-with? fname ".gz")
            (GZIPInputStream. %)
            %))
        xml/parse)
    node))

;; this does not do enough type casting, ints come in as strings and no date parsing yet
;; also have not yet done any parsing
(defn load-cfg
  [fname]
  (let [file (java.io.File. fname)
        subdir (.getParent file)
        z (-> file
              (xml/parse)
              (zip/xml-zip))
        ;; defines used later for substitution during parsing
        defines (->> (xml-> z :defines :define zip/node)
                     (map :attrs)
                     (map (juxt :name
                                :value))
                     (into {}))
        replace-defines (let [pat (->> (keys defines)
                                       (s/join "|" )
                                       (format "\\$(%s)")
                                       (re-pattern))]
                          (fn [s] (loop [s s]
                                    (if-let [m (second (re-find pat s))]
                                      (recur (s/replace s (str "$" m) (defines m)))
                                      s))))]
  ;;;; This is the long map or config to return ;;;;
  {:defines (into {} (map (fn [[k v]]  ;; recursive required?
                            [k (replace-defines v)])
                          defines))
   ;; partial work on params no parse int on values yet
   :params (->> (xml-> z :parameters :parameter zip/node)
                (map :attrs)
                (map (juxt (comp keyword :name)
                           (comp replace-defines :value)))
                (into {}))
   :rates (->> (xml-> z :interest :rate zip/node)
               (map :attrs)
               (map (juxt (comp keyword :t)
                          (comp parse-pct :r)))
               (into {}))
   :ratings (->> (xml-> z :ratings :rating zip/node)
               (map :attrs)
               (map :name))
   :trans (->> (xml-> z
                      :transitions
                      :transition
                      #(:attrs (zip/node %)))
               (group-by :from )
               (map  (fn [[from row]]
                       (->> row
                            (map (juxt (comp keyword :to)
                                       (comp parse-pct :value)))
                            (into {:from-to (keyword from)}))))
               (i/to-dataset))
   :factors (->> (xml-> z :factors :factor zip/node)
                 (map :attrs)
                 (map (juxt (comp keyword :name)
                            #_(comp #_replace-defines :value)
                            identity))
                 (into {}))
   :corrmx (let [f1f2v (->> (xml-> z
                                   :correlations
                                   :correlation
                                   (comp :attrs zip/node))
                            (map (juxt (comp keyword :factor1)
                                       (comp keyword :factor2)
                                       (comp parse-pct replace-defines :value))))
                 col-names (distinct (mapcat (partial take 2) f1f2v))
                 cm (into {} (map #(vector %1 {%1 1}) col-names))  ;diagonal=1
                 addv (fn [m [f1 f2 v]] (-> m
                                            (assoc-in [f1 f2] v)
                                            (assoc-in [f2 f1] v)))]
             (i/to-dataset (map second (reduce addv cm f1f2v))))
   :segmentations (->> (xml-> z
                              :segmentations
                              :segmentation
                              snode->slist)
                       (map (juxt (comp keyword :name)
                                  identity))
                       (into {}))
   :portfolio (->> (xml-> z
                          :portfolio
                          #(zip/edit % include subdir) ;; should others process includes?
                          :obligor
                          onode->alist)
                   i/to-dataset)}))

(def fname "resources/ccruncher/test03.xml")
(load-cfg fname)

(comment
  (def fname "resources/ccruncher/phil3.xml")
  (def z (-> (java.io.File. fname)
             (xml/parse)
             (zip/xml-zip)))
  ;; this returns "values" as a sub element (this is a nested structure)
  (xml-> z
         :portfolio
         :obligor
         :asset
         (fn [n]
           (let [values (xml-> n
                               :data
                               :values
                               (comp :attrs zip/node))]
             (-> n
                 zip/node
                 :attrs
                 ;; (assoc :data (i/to-dataset values))
                 (assoc :ead (mapv (comp parse-pct :ead) values))
                 (assoc :lgd (mapv (comp parse-pct :lgd) values))))))
 )
