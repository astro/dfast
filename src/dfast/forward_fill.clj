(ns dfast.forward-fill)

(defn forward-fill
  [& keys]
  (fn [rf]
    (let [prev (volatile! {})]
     (fn 
       ([] (rf))
       ([result] (rf result))
       ([result  vals]
        (->> (for [key keys
                   :let [val (key vals)]
                   :when val]
               [key val])
             (vswap! prev into)
             (merge vals)
             (rf result)))))))
;; (def maps [{:a 1 :b 1} {:a 2 :c 2} {:a 3 :d 3}])
;; (sequence (forward-fill :a :b :c) maps)
;; (transduce (comp (forward-fill :a) (map :a)) + maps)
;; => 5


;; non-transducer  not as useful but simpler 
;; forward fills all keys
(defn forward-fill-all
  ([coll] (forward-fill-all coll {}))
  ([coll fill-map] (if (seq coll)
                     (let [ff-map (into fill-map (first coll))]
                       (lazy-seq (cons ff-map (forward-fill-all (next coll) ff-map)))))))

