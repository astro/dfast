(ns example1
  (:require [dfast.io :refer [load-loans save-loans report
                              load-recovery-rates load-transition-matrix]]
            [dfast.loans :refer :all]
            [incanter.io :as io]
            [incanter.core :as i]))

;; Most of this was repel interactive and not curent.
;; example3 has a complete runthrough more compact

;; (set! *warn-on-reflection* true)

;; read in some data from files

;; forward interest rate curves
(def curves  (-> "resources/curves4.csv"
                 (io/read-dataset :header true)
                 (index-dataset)
                 (ccc-c)))

;; read in some data from ifiles
;; (def ll (load-loans             "ll.csv"))
(def rr (load-recovery-rates    "resources/RecoveryRate.csv"))
(def tm (load-transition-matrix "resources/Transition_matrix.csv"))


;; not sure we'll use this
(def r (io/read-dataset "resources/old_rtg.csv" :header true))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some basic transformations on the data

;; matrix version of transition matrix
(def tmx (i/to-matrix tm))

;; discount factors
(def rtg->df (curves->df curves))

;; Probability of Default (PD) for each rating
(def rtg->pd (make-rtg->pd tm))

;; Loss Given Default (LGD) means used for Expected Loss (EL)
(def sec->lgd (make-sec->lgd rr))

;; not sure where this belongs
(defn init-rtg-mx [tm l]
  (let [rtg-idx (-> tm :index  ((:rating l)))
        zeros (vec (repeat 8 0))]
    (->> (assoc zeros rtg-idx 1)
         (i/matrix)
         (i/trans)
         (assoc l :rtg-mx))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this section uses clojure.spec to generate values (i.e. loans)
(require '[clojure.spec.alpha :as s])
(def ll (->> (s/exercise :dfast.loans/loan 1000)
                          (map (comp map->amortized-loan second))
                          (filter #(not= :dfast.loans/default (:rating %)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sample calcs that "don't change anything"
(i/sel tm :rows (map #((:index tm) (:rating %)) ll))

(sequence (comp (filter :rating)  ;; nillable  I think not
                (filter #((:index tm) (:rating %))) ;missing ratings like c
                (map (partial init-rtg-mx tm))
                #_(map (partial transform-ratings tmx)) ;; TODO: why error here?
                #_(map :rtg-mx))                        ;; used to work now it's a link error 
          ll)

#_(->> (i/mmult (i/to-matrix (i/sel r  :except-cols 0))
                (i/to-matrix (i/sel tm :except-cols 0)))
     (i/dataset (next (i/col-names r)))
     #_(i/conj-cols (i/$ 0 r)))

;; select rows by index 
(i/sel tm :cols [:dfast.loans/aaa :dfast.loans/aa :dfast.loans/a] :rows (map (:index tm) [:dfast.loans/aaa :dfast.loans/aa]))
;; show the index  TODO: wrong since index keys may not match rows
;; (i/conj-cols (i/dataset [:index] (keys (:index tm))) tm)

(transduce (comp (map make-pmt)
                 (map make-pmt)
                 (map (partial pv-loan rtg->df))) + ll)

;; get sample recover rates for loans
;; transducer version? or eduction? dunno
(->> ll
     (map :security)
     (map (fn [s] ((:index rr) s)))
     (i/sel rr :cols [:Mean :StdDev] :rows)
     :rows
     (map sample-recovery))

;; get loans with sample trasnformations from a transition matrix
(map (partial transform-rating tm) ll)

;; transform and count number of defaults
(->> ll
     (map (partial transform-rating tm))
     (filter #(= :dfast.loans/default (:rating %)))
     (count))

;; expected loss on a loan l rr rtg->pd
(defn el [l]
  (* (:bal l)
     (rtg->pd (:rating l))
     (sec->lgd (:security l)))) 

(defn sl [l]
  (let [rr-row (nth (:rows rr) ((:security l) (:index rr)))]
   (* (:bal l)
      (- 1 (sample-recovery rr-row)))))

;; expected loss on portfolio
(transduce (map el) + ll)

;; sample loss on portfolio
(transduce (comp (map (partial transform-rating tm))
                 (filter (fn [l] (= :dfast.loans/default (:rating l))))
                 (map sl))
           + ll)


;; sample loss should converge to el for large num-path
(def losses
  (let [num-path 10000
        xform-rtg (transform-rating3 tm)]
    (repeatedly num-path
                #(transduce (comp (map xform-rtg)
                                  (filter (fn [l] (= :dfast.loans/default (:rating l))))
                                  (map sl))
                            + ll))))
;; this is to force eval and get a sense of timing (~100K per second initially)
;; now ~300k per second after moving from transform-matrix to transform-matrix2
(time (count losses))

(report losses)
;; [::CB ::IB ::BB ::CORP ::AUTO ::CCARD ::MTG] 

;; not great but an attemp at saving to csv
(->> ll
     (map #(into {} %))  ;; turn records into maps
     (map #(dissoc % :cash-flows))  ;; remove cash flows
     (map #(update % :rating name)) ;; change namespaced keywords to strings
     (map #(update % :security name))
     i/to-dataset
     (#(i/save % "lll.csv")))

;; now let's try to load it back up
(def lll (load-loans "lll.csv"))
